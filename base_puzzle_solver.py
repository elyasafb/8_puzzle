class BasePuzzleSolver(object):
    def dfs(self):
        raise NotImplementedError()

    def bfs(self):
        raise NotImplementedError()

    def a_star(self):
        raise NotImplementedError()
