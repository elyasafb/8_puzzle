from itertools import permutations
from random import randint

from base_puzzle_builder import BasePuzzleBuilder
from n_puzzle import NPuzzle


class PuzzleBuilder(BasePuzzleBuilder):

    def generate_puzzle(self, size=3):
        all_permutations = list(permutations(range(size ** 2), size ** 2))
        rand_index = randint(0, len(all_permutations))
        initial_state = list(all_permutations[rand_index])
        return NPuzzle(initial_state)

    def create_puzzle_from_arr(self, initial_state):
        return NPuzzle(initial_state)
