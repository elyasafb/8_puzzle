class BasePuzzle(object):
    def __init__(self):
        self._action_dict = {
            'up': 'move_up',
            'down': 'move_down',
            'left': 'move_left',
            'right': 'move_right',
        }

    def _move_left(self):
        raise NotImplementedError()

    def _move_right(self):
        raise NotImplementedError()

    def _move_up(self):
        raise NotImplementedError()

    def _move_down(self):
        raise NotImplementedError()
