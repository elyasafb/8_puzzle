from base_puzzle_builder import BasePuzzleBuilder
from base_puzzle_solver import BasePuzzleSolver
from n_puzzle import NPuzzle

import heapq


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]


class PuzzleSolver(BasePuzzleSolver):
    def __init__(self, puzzle_builder: BasePuzzleBuilder):
        self._puzzle_builder = puzzle_builder
        self._initial_states = None
        self._states = []
        self._node_expanded = 0
        self._explored_states = set()

    def dfs(self):
        while self._states:
            curr_state = self._states.pop()
            if self._is_finish(curr_state):
                return curr_state, curr_state.get_all_moves(), len(curr_state.get_all_moves()), self._node_expanded
            self._node_expanded += 1
            for next_state in curr_state.get_all_children():
                self._add_state(next_state)

    def bfs(self):
        while self._states:
            curr_state = self._states.pop(0)
            if self._is_finish(curr_state):
                return curr_state, curr_state.get_all_moves(), len(curr_state.get_all_moves()), self._node_expanded
            self._node_expanded += 1
            for next_state in curr_state.get_all_children():
                self._add_state(next_state)

    @staticmethod
    def heuristic(state: NPuzzle):
        return state.heuristic()

    def a_star(self):
        frontier = PriorityQueue()
        frontier.put(self._initial_states, 0)
        came_from = {}
        cost_so_far = {}
        came_from[self._initial_states] = None
        cost_so_far[self._initial_states] = 0

        while not frontier.empty():
            current = frontier.get()

            if self._is_finish(current):
                return current, current.get_all_moves(), len(current.get_all_moves()), self._node_expanded

            for next_state in current.get_all_children():
                new_cost = cost_so_far[current] + 1
                if next_state not in cost_so_far or new_cost < cost_so_far[next_state]:
                    cost_so_far[next_state] = new_cost
                    priority = new_cost + self.heuristic(next_state)
                    frontier.put(next_state, priority)
                    came_from[next_state] = current
        return False

    def start(self, algorithm, initial_state=None):
        if not initial_state:
            self._initial_states = self._puzzle_builder.generate_puzzle()
        else:
            self._initial_states = self._puzzle_builder.create_puzzle_from_arr(initial_state)
        self._add_state(self._initial_states)
        return getattr(self, algorithm)()

    @staticmethod
    def _move(move, state: NPuzzle):
        return state.move(move)

    @staticmethod
    def _is_finish(state: NPuzzle):
        return state.is_goal_state()

    def _add_state(self, new_state):
        if hash(new_state) not in self._explored_states:
            self._explored_states.add(hash(new_state))
            self._states.append(new_state)
