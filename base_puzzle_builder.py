class BasePuzzleBuilder(object):
    def generate_puzzle(self, size=3):
        raise NotImplementedError()

    def create_puzzle_from_arr(self, initial_state):
        raise NotImplementedError()
