import math

from base_puzzle import BasePuzzle


class InvalidMovement(Exception):
    pass


class NPuzzle(BasePuzzle):

    def __init__(self, initial_state: list, parent=None, move=None):
        super(BasePuzzle, self).__init__()
        self._parent = parent
        self._initial_state = initial_state
        self._width = int(math.sqrt(len(initial_state)))
        self._opposite_moves = {'up': 'down', 'left': 'right', 'down': 'up', 'right': 'left'}
        self._move_to_function = {'down': '_move_down', 'up': '_move_up', 'right': '_move_right', 'left': '_move_left'}
        self._move = move

    def _move_right(self):
        new_state = self._initial_state.copy()
        empty_index = self._initial_state.index(0)
        if empty_index % self._width == self._width - 1:
            raise InvalidMovement(f'Try move left state \n{self.get_string_state()}')
        new_state[empty_index] = self._initial_state[empty_index + 1]
        new_state[empty_index + 1] = 0
        return NPuzzle(new_state, self, 'right')

    def _move_left(self):
        new_state = self._initial_state.copy()
        empty_index = self._initial_state.index(0)
        if empty_index % self._width == 0:
            raise InvalidMovement(f'Try move right state \n{self.get_string_state()}')
        new_state[empty_index] = self._initial_state[empty_index - 1]
        new_state[empty_index - 1] = 0
        return NPuzzle(new_state, self, 'left')

    def _move_down(self):
        new_state = self._initial_state.copy()
        empty_index = self._initial_state.index(0)
        if int(empty_index / self._width) == self._width - 1:
            raise InvalidMovement(f'Try move up state \n{self.get_string_state()}')
        new_state[empty_index] = self._initial_state[empty_index + self._width]
        new_state[empty_index + self._width] = 0
        return NPuzzle(new_state, self, 'down')

    def _move_up(self):
        new_state = self._initial_state.copy()
        empty_index = self._initial_state.index(0)
        if int(empty_index / self._width) == 0:
            raise InvalidMovement(f'Try move down state \n{self.get_string_state()}')
        new_state[empty_index] = self._initial_state[empty_index - self._width]
        new_state[empty_index - self._width] = 0
        return NPuzzle(new_state, self, 'up')

    def move(self, move):
        move_fun = self._move_to_function[move]
        return getattr(self, move_fun)()

    def get_string_state(self):
        res = ''
        to_print = '| {num} '
        for i in range(self._width):
            tmp = []
            for j in range(self._width):
                tmp.append(to_print.format(num=self._initial_state[i * self._width + j]))
            tmp[-1] = tmp[-1] + '|\n'
            res += ''.join(tmp)
        return res

    def is_goal_state(self):
        for i in range(len(self._initial_state) - 1):
            if i != self._initial_state[i]:
                return False
        return True

    def __hash__(self):
        return hash(''.join([str(i) for i in self._initial_state]))

    def __str__(self):
        return self.get_string_state()

    def __repr__(self):
        return self.get_string_state()

    def get_all_moves(self):
        parent = self.get_parent()
        res = [self.get_move()]
        while parent and parent.get_move():
            res.insert(0, parent.get_move())
            parent = parent.get_parent()
        return res

    def get_move(self):
        return self._move

    def get_parent(self):
        return self._parent

    def get_available_moves(self):
        moves = ['up', 'left', 'down', 'right']
        all_moves = {'down', 'right', 'left', 'up'}
        all_moves = all_moves - {self._opposite_moves.get(self._move)}
        if self._empty_in_first_row():
            all_moves -= {'up'}
        if self._empty_in_last_row():
            all_moves -= {'down'}
        if self._empty_in_first_col():
            all_moves -= {'left'}
        if self._empty_in_last_col():
            all_moves -= {'right'}
        res = []
        for move in moves:
            if move in all_moves:
                res.append(move)
        return res

    def _empty_in_first_row(self):
        idx = self._initial_state.index(0)
        return int(idx / self._width) == 0

    def _empty_in_last_row(self):
        idx = self._initial_state.index(0)
        return int(idx / self._width) == self._width - 1

    def _empty_in_first_col(self):
        idx = self._initial_state.index(0)
        return idx % self._width == 0

    def _empty_in_last_col(self):
        idx = self._initial_state.index(0)
        return idx % self._width == self._width - 1

    def get_all_children(self):
        children = []
        for move in self.get_available_moves():
            children.append(self.move(move))
        return children

    def heuristic(self):
        temp = 0
        for i in range(0, self._width ** 2):
            if self._initial_state[i] != i and self._initial_state[i] != '0':
                temp += 1
        return temp

    def __gt__(self, other):
        return self.heuristic() > other.heuristic()

    def __lt__(self, other):
        return self.heuristic() < other.heuristic()

    def __eq__(self, other):
        return self.heuristic() == other.heuristic()

    def __ge__(self, other):
        return self > other or self == other

    def __le__(self, other):
        return self < other or self == other
