from puzzle_builder import PuzzleBuilder
from puzzle_solver import PuzzleSolver

OPTIONS = ['bfs , dfs', 'a_star']

if __name__ == '__main__':
    print(PuzzleSolver(PuzzleBuilder()).start('a_star', [1, 2, 5, 3, 4, 0, 6, 7, 8]))
